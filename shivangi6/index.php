<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>SHIVANGI PATHAK A MOUNTAINEER</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

  <!-- Favicons -->
  <!--
<link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">
 -->

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style-sky-blue.css" rel="stylesheet">


  <!-- =======================================================
    Theme Name: DevFolio
    Theme URL: https://bootstrapmade.com/devfolio-bootstrap-portfolio-html-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body id="page-top">

  <!--/ Nav Star /-->
  <nav class="navbar navbar-b navbar-trans navbar-expand-md fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll" href="#page-top">Shivangi Pathak</a>
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll active" href="#home">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="#about">About Me</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="#awards">Awards</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="#achievements">Achievements</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="#media">Media</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="#blog">Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="#gallery">Gallery</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll" href="#contact">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!--/ Nav End /-->

  <!--/ Intro Skew Star /-->
  <div id="home" class="intro route bg-image" style="background-image: url(img/intro-bg1.jpg)">
    <div class="overlay-itro"></div>
    <div class="intro-content display-table">
      <div class="table-cell">
        <div class="container">
          <!--<p class="display-6 color-d">Hello, world!</p>-->
          <h1 class="intro-title mb-4">Shivangi Pathak</h1>
          <p class="intro-subtitle"><span class="text-slider-items">Eagle Of Mountains</span><strong class="text-slider"></strong></p>
          <!-- <p class="pt-3"><a class="btn btn-primary btn js-scroll px-4" href="#about" role="button">Learn More</a></p> -->
        </div>
      </div>
    </div>
  </div>
  <!--/ Intro Skew End /-->

  <section id="about" class="about-mf sect-pt4 route">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="box-shadow-full">
            <div class="row">
              <div class="col-md-12">
                <div class="about-me pt-4 pt-md-0">
                  <div class="title-box-2">
                    <h5 class="title-left">
                      WHO IS SHIVANGI PATHAK?
                    </h5>
                  </div>
                  <!-- <h5 style="color:#48abec">WHO IS SHIVANGI PATHAK?</h5> -->
                  <div class="row">
                    <div class="col-md-12">
                      <p class="lead">
                        A mountaineer who has made India proud with her ground breaking achievements, Shivangi Pathak is ‘The Eagle of Mountains’ -One of the youngest girls to Summit Mt. Everest from Haryana state to represent India in Mountaineering in 2018
                      </p>
                      <p class="lead">
                        A Mountain Climber, Resident of Hisar district, she is surprisingly naughty yet humble about all her achievements. Named as Shivangi Pathak at birth, also known as “The Eagle of Mountains” as she marked her journey towards the Everest as a way to fulfil her mother’s wish. Born into a middle class joint family, her father is an Electronics Merchant and mother is a house wife. As a school girl she used to play and participate in a variety of extra-curricular activities and had a keen interest in Dancing, but surprisingly not climbing!
                      </p>
                      <p class="lead">
                        Still taking up a sport considered “masculine” by social standards was no easy task for the young girl who comes from a conservative area. But Shivangi was not someone to be discouraged, and travelled to The Jawahar Institute of Mountaineering and Winter Sports (JIM&WS) at Pahalgam, Jammu & Kashmir to train in Mountain Climbing. Today her success is for all to see! Of course, there is more to Shivangi Pathak than just professional success - her vision is to empower & educate women and young girls not only from her district Hisar but also nationwide
                      </p>
                    </div>
                    <!-- <div class="col-md-4">
                      <img src="img/s-p2.JPG" class="border p-2" alt="" height="" width="100%">
                      <figcaption class="text-center"> <h6 class="mt-2">Inspiration for Mountaineering </h6> </figcaption>
                    </div> -->
                  </div>

                </div>
              </div>
              <!-- <div class="col-md-4">
                <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <div class="about-img ">
                      <img src="img/s-p2.JPG" class="img-fluid rounded b-shadow-a" alt="">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="skill-mf" style="background-color: #f5f5f5;border: 1px solid #ddd; padding: 10px;">
                      <p class="title-s text-center">Media</p>
                      <div class="pb-2 medialink">
                        <a target="blank" href="https://economictimes.indiatimes.com/magazines/panache/16-year-old-shivangi-pathak-becomes-youngest-indian-girl-to-climb-mount-everest/articleshow/64271613.cms">
                         <span>1).</span>  16-year-old Shivangi Pathak becomes youngest Indian girl to climb Mount Everest </a>
                      </div>
                      <div class="pb-2 medialink">
                        <a target="blank" href="https://www.whatsuplife.in/shivangi-pathak-climbs-mt-everest/">
                         <span>2).</span>Shivangi Pathak, The Youngest Indian Woman to Scale Mt. Everest</a>
                      </div>
                      <div class="pb-2 medialink">
                        <a target="blank" href="https://www.financialexpress.com/india-news/on-top-of-the-world-shivangi-pathak-16-becomes-youngest-indian-woman-to-climb-mount-everest/1175641/">
                         <span>3).</span>On top of the world! Shivangi Pathak,</a>
                      </div>
                      <div class="pb-2 medialink">
                        <a target="blank" href="https://www.quora.com/Who-is-the-first-Indian-woman-to-scale-the-Mt-Everest-Shivangi-Pathak-or-Malavath-Poorna">
                         <span>4).</span> Who is the first Indian woman to scale the Mt. Everest</a>
                      </div>
                      <div class="pb-2 medialink">
                        <a target="blank" href="https:/https://en.everybodywiki.com/Shivangi_Pathak">
                         <span>5).</span>  Shivangi Pathak (10 July 2001) is an Indian mountaineer from Hisar </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<!-- <section id="service" class="services-mf route">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="title-box text-center">
          <h3 class="title-a">
            Media
          </h3>
          <p class="subtitle-a">
            Early life and Family Background
          </p>
          <div class="line-mf"></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="service-box">
          <div class="service-ico">
            <span class="ico-circle"><i class="ion-monitor"></i></span>
          </div>
          <div class="service-content">
            <h2 class="s-title">Web Design</h2>
            <p class="s-description text-center">
              She was born in Hisar District, Haryana, India to Mr. Mr. Rajesh Pathak who is an Electronics Businessman and Mrs. Aarti Pathak. She was named as Shivangi Pathak at birth, but now she is known as “The Eagle of Mountains” as he entered the world of professional mountaineering by climbing the Mt. Everest from the Nepal side on 16th May, 2018
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="service-box">
          <div class="service-ico">
            <span class="ico-circle"><i class="ion-code-working"></i></span>
          </div>
          <div class="service-content">
            <h2 class="s-title">Web Development</h2>
            <p class="s-description text-center">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni adipisci eaque autem fugiat! Quia,
              provident vitae! Magni
              tempora perferendis eum non provident.
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="service-box">
          <div class="service-ico">
            <span class="ico-circle"><i class="ion-camera"></i></span>
          </div>
          <div class="service-content">
            <h2 class="s-title">Photography</h2>
            <p class="s-description text-center">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni adipisci eaque autem fugiat! Quia,
              provident vitae! Magni
              tempora perferendis eum non provident.
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="service-box">
          <div class="service-ico">
            <span class="ico-circle"><i class="ion-android-phone-portrait"></i></span>
          </div>
          <div class="service-content">
            <h2 class="s-title">Responsive Design</h2>
            <p class="s-description text-center">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni adipisci eaque autem fugiat! Quia,
              provident vitae! Magni
              tempora perferendis eum non provident.
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="service-box">
          <div class="service-ico">
            <span class="ico-circle"><i class="ion-paintbrush"></i></span>
          </div>
          <div class="service-content">
            <h2 class="s-title">Graphic Design</h2>
            <p class="s-description text-center">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni adipisci eaque autem fugiat! Quia,
              provident vitae! Magni
              tempora perferendis eum non provident.
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="service-box">
          <div class="service-ico">
            <span class="ico-circle"><i class="ion-stats-bars"></i></span>
          </div>
          <div class="service-content">
            <h2 class="s-title">Marketing Services</h2>
            <p class="s-description text-center">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni adipisci eaque autem fugiat! Quia,
              provident vitae! Magni
              tempora perferendis eum non provident.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> -->


  <!--/ Section Awards Star /-->

  <div id="awards" class="section-counter paralax-mf bg-image cus-paralax-mf" style="background-image: url(img/award-bg.jpg)">
    <div class="overlay-mf award-mf"></div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-box text-center">
            <h3 class="title-a text-white">
              Awards
            </h3>
            <div class="line-mf-w"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4 col-lg-4">
          <div class="card card-blog">
            <div class="card-img award-card-img">
              <img src="img/pf-small4.jpg" alt="" class="img-fluid">
            </div>
            <div class="card-body">
              <h6 class="card-title award_tital">Bal shakti Puraskar By President Ram Nath Kovind</h6>
            </div>
          </div>
        </div>
        <div class="col-sm-4 col-lg-4">
          <div class="card card-blog">
            <div class="card-img award-card-img">
              <img src="img/award.cm.jpg" alt="" class="img-fluid">
            </div>
            <div class="card-body">
              <h6 class="card-title award_tital">Certificate of brave by honorable Chief Minister Of Haryana</h3>
            </div>
          </div>
        </div>
        <div class="col-sm-4 col-lg-4">
          <div class="card card-blog">
            <div class="card-img award-card-img">
            <img src="img/award-honda.jpg" alt="" class="img-fluid">
            </div>
            <div class="card-body">
              <h6 class="card-title award_tital">Awarded by Honda Two-wheeler as a sponsorship of Elbrus</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Section Awards End -->

  <!--/ Section Achievements Star /-->
  <section id="achievements" class="portfolio-mf sect-pt4 route">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-box text-center">
            <h3 class="title-a">
              Achievements
            </h3>
            <p class="subtitle-a">
              Inspiration for Mountaineering : Shivangi Pathak becomes youngest Indian woman to scale Everest
            </p>
            <div class="line-mf"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="work-box">
            <a href="img/pf-big1.JPG" data-lightbox="gallery-mf">
            <div class="work-img">
              <img src="img/pf-small1.jpg" alt="" class="img-fluid">
            </div>
            </a>
            <div class="work-content">
              <div class="row">
                <div class="col-sm-10">
                  <h2 class="w-title">Mt Everest (8848 m) Summiteer</h2>
                </div>
                <div class="col-sm-2">
                  <a href="#" data-toggle="modal" data-target="#EverestModal">
                    <div class="w-like">
                      <span class="ion-ios-plus-outline"></span>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="work-box">
            <a href="img/pf-big2.JPG" data-lightbox="gallery-mf">
            <div class="work-img">
              <img src="img/pf-small2.jpg" alt="" class="img-fluid">
            </div>
            </a>
            <div class="work-content">
              <div class="row">
                <div class="col-sm-10">
                  <h2 class="w-title">Mt Kilimanjaro (5895m) Summiteer</h2>
                </div>
                <div class="col-sm-2">
                  <a href="#" data-toggle="modal" data-target="#kilimanjaroModal">
                  <div class="w-like">
                    <span class="ion-ios-plus-outline"></span>
                  </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="work-box">
            <a href="img/pf-big3.jpg" data-lightbox="gallery-mf">
            <div class="work-img">
              <img src="img/pf-small3.jpg" alt="" class="img-fluid">
            </div>
            </a>
            <div class="work-content">
              <div class="row">
                <div class="col-sm-10">
                  <h2 class="w-title">Mt Elbrus (5642m) Summiter</h2>
                </div>
                <div class="col-sm-2">
                  <a href="#" data-toggle="modal" data-target="#ElbrusModal">
                  <div class="w-like">
                    <span class="ion-ios-plus-outline"></span>
                  </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="work-box">
            <a href="img/pf-big4.jpg" data-lightbox="gallery-mf">
            <div class="work-img">
              <img src="img/pf-small4.jpg" alt="" class="img-fluid">
            </div>
            </a>
            <div class="work-content">
              <div class="row">
                <div class="col-sm-10">
                  <h2 class="w-title">Awarded By Bal shakti Puraskar By President Ram Nath Kovind </h2>
                </div>
                <div class="col-sm-2">
                  <a href="#" data-toggle="modal" data-target="#PresidentModal">
                  <div class="w-like">
                    <span class="ion-ios-plus-outline"></span>
                  </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="work-box">
              <a href="img/pf-big5.jpg" data-lightbox="gallery-mf">
              <div class="work-img">
                <img src="img/pf-small5.jpg" alt="" class="img-fluid">
              </div>
              </a>
              <div class="work-content">
                <div class="row">
                  <div class="col-sm-10">
                    <h2 class="w-title">Honoured By Prime Minister Narendra Modi</h2>
                  </div>
                  <div class="col-sm-2">
                    <a href="#" data-toggle="modal" data-target="#PrimeMinisterModal">
                    <div class="w-like">
                      <span class="ion-ios-plus-outline"></span>
                    </div>
                    </a>
                  </div>
                </div>
              </div>

          </div>
        </div>
        <div class="col-md-4">
          <div class="work-box">
            <a href="img/achivement-cm-big.jpg" data-lightbox="gallery-mf">
            <div class="work-img">
              <img src="img/achivement-cs-small.jpg" alt="" class="img-fluid">
            </div>
            </a>
            <div class="work-content">
              <div class="row">
                <div class="col-sm-10">
                  <h2 class="w-title">Honoured And Appreciated by Chief Minister haryana</h2>
                </div>
                <div class="col-sm-2">
                  <a href="#" data-toggle="modal" data-target="#CmharyanaModal">
                    <div class="w-like">
                      <span class="ion-ios-plus-outline"></span>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="work-box">
            <a href="img/achivement-Niwasji-big.jpeg" data-lightbox="gallery-mf">
            <div class="work-img">
              <img src="img/achivement-Niwasji-small.jpg" alt="" class="img-fluid">
            </div>
            </a>
            <div class="work-content">
              <div class="row">
                <div class="col-sm-10">
                  <h2 class="w-title">Honoured & appreciated by Shri Niwas ji (National Joint organising Secretary ABVP)</h2>
                </div>
                <div class="col-sm-2">
                  <a href="#" data-toggle="modal" data-target="#NiwasjiModal">
                    <div class="w-like">
                      <span class="ion-ios-plus-outline"></span>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="work-box">
            <a href="img/achivement-UnionM-big.jpeg" data-lightbox="gallery-mf">
            <div class="work-img">
              <img src="img/achivement-UnionM-small.jpg" alt="" class="img-fluid">
            </div>
            </a>
            <div class="work-content">
              <div class="row">
                <div class="col-sm-10">
                  <h2 class="w-title">“Jindagi Na Milegi Dobara “ award by Honourable Union Minister Shri Rajyavardhan Singh Rathore</h2>
                </div>
                <div class="col-sm-2">
                  <a href="#" data-toggle="modal" data-target="#Modal">
                    <div class="w-like">
                      <span class="ion-ios-plus-outline"></span>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="work-box">
            <a href="img/achivement-Kasturi-big.jpeg" data-lightbox="gallery-mf">
            <div class="work-img">
              <img src="img/achivement-Kasturi-small.jpg" alt="" class="img-fluid">
            </div>
            </a>
            <div class="work-content">
              <div class="row">
                <div class="col-sm-10">
                  <h2 class="w-title">Received first Sponsorship by Kasturi Memorial Trust Haryana</h2>
                </div>
                <div class="col-sm-2">
                  <a href="#" data-toggle="modal" data-target="#Modal">
                    <div class="w-like">
                      <span class="ion-ios-plus-outline"></span>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="work-box">
            <a href="img/achivement-Kasturi2-big.jpeg" data-lightbox="gallery-mf">
            <div class="work-img">
              <img src="img/achivement-Kasturi2-small.jpg" alt="" class="img-fluid">
            </div>
            </a>
            <div class="work-content">
              <div class="row">
                <div class="col-sm-10">
                  <h2 class="w-title">Honoured & Appreciated by Kasturi Memorial Trust Haryana</h2>
                </div>
                <div class="col-sm-2">
                  <a href="#" data-toggle="modal" data-target="#Modal">
                    <div class="w-like">
                      <span class="ion-ios-plus-outline"></span>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
  <!--/ Section Achievements End /-->

  <!-- Section media Star /-->
  <section id="media" class="media">
   <div class="container">
     <div class="row">
       <div class="col-sm-12">
         <div class="title-box text-center">
           <h3 class="title-a">
             Media
           </h3>
           <div class="line-mf"></div>
         </div>
       </div>
     </div>
       <div class="row">
         <div class="col-sm-12 col-md-6 col-lg-6  py-0 pl-3 pr-1 featcard">
            <div id="featured" class="carousel slide carousel-fade" data-ride="carousel">
               <div class="carousel-inner">
                  <div class="carousel-item active">
                     <div class="card bg-dark text-white">
                        <img class="card-img img-fluid" src="img/pf-small1.jpg" alt="">
                        <div class="card-img-overlay d-flex linkfeat">
                           <a href="https://economictimes.indiatimes.com/magazines/panache/16-year-old-shivangi-pathak-becomes-youngest-indian-girl-to-climb-mount-everest/articleshow/64271613.cms" class="align-self-end">
                              <h4 class="card-title cus-card-title">Shivangi Pathak becomes youngest Indian girl to climb Mount Everest</h4>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <div class="card bg-dark text-white">
                        <img class="card-img img-fluid" src="img/pf-small2.jpg" alt="">
                        <div class="card-img-overlay d-flex linkfeat">
                           <a href="https://www.whatsuplife.in/shivangi-pathak-climbs-mt-everest/" class="align-self-end">
                              <h4 class="card-title cus-card-title  ">Shivangi Pathak, The Youngest Indian Woman to Scale Mt. Everest</h4>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <div class="card bg-dark text-white">
                        <img class="card-img img-fluid" src="img/pf-small3.jpg" alt="">
                        <div class="card-img-overlay d-flex linkfeat">
                           <a href="https://www.financialexpress.com/india-news/on-top-of-the-world-shivangi-pathak-16-becomes-youngest-indian-woman-to-climb-mount-everest/1175641/" class="align-self-end">
                              <h4 class="card-title cus-card-title">On top of the world! Shivangi Pathak</h4>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="carousel-item">
                     <div class="card bg-dark text-white">
                        <img class="card-img img-fluid" src="img/pf-small4.jpg" alt="">
                        <div class="card-img-overlay d-flex linkfeat">
                           <a href="https://www.quora.com/Who-is-the-first-Indian-woman-to-scale-the-Mt-Everest-Shivangi-Pathak-or-Malavath-Poorna" class="align-self-end">
                              <h4 class="card-title cus-card-title">Who is the first Indian woman to scale the Mt. Everest </h4>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-6 py-0 px-1 d-none d-lg-block">
            <div class="row">
               <div class="col-6 pb-2 mg-1	">
                  <div class="card bg-dark text-white">
                     <img class="card-img img-fluid" src="img/pf-small1.jpg" alt="">
                     <div class="card-img-overlay d-flex">
                        <a href="https://economictimes.indiatimes.com/magazines/panache/16-year-old-shivangi-pathak-becomes-youngest-indian-girl-to-climb-mount-everest/articleshow/64271613.cms" class="align-self-end">
                           <h6 class="card-title cus-card-title">Shivangi Pathak becomes youngest Indian girl to climb Mount Everest </h6>
                        </a>
                     </div>
                  </div>
               </div>
               <div class="col-6 pb-2 mg-2	">
                  <div class="card bg-dark text-white">
                     <img class="card-img img-fluid" src="img/pf-small5.jpg" alt="">
                     <div class="card-img-overlay d-flex">
                       <a href="https://www.whatsuplife.in/shivangi-pathak-climbs-mt-everest/" class="align-self-end">
                          <h6 class="card-title cus-card-title  ">Shivangi Pathak, The Youngest Indian Woman to Scale Mt. Everest</h6>
                       </a>
                     </div>
                  </div>
               </div>
               <div class="col-6 pb-2 mg-3	">
                  <div class="card bg-dark text-white">
                     <img class="card-img img-fluid" src="img/pf-small6.jpg" alt="">
                     <div class="card-img-overlay d-flex">
                       <a href="https://www.financialexpress.com/india-news/on-top-of-the-world-shivangi-pathak-16-becomes-youngest-indian-woman-to-climb-mount-everest/1175641/" class="align-self-end">
                          <h6 class="card-title cus-card-title">On top of the world! Shivangi Pathak</h6>
                       </a>
                     </div>
                  </div>
               </div>
               <div class="col-6 pb-2 mg-4	">
                  <div class="card bg-dark text-white">
                     <img class="card-img img-fluid" src="img/pf-small3.jpg" alt="">
                     <div class="card-img-overlay d-flex">
                       <a href="https://www.quora.com/Who-is-the-first-Indian-woman-to-scale-the-Mt-Everest-Shivangi-Pathak-or-Malavath-Poorna" class="align-self-end">
                          <h6 class="card-title cus-card-title">Who is the first Indian woman to scale the Mt. Everest </h6>
                       </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>
  </section>
  <!-- Section media Star /-->

  <!--/ Section personal detail /-->
  <div class="testimonials paralax-mf bg-image cus-paralax-mf sect-mt4" style="background-image: url(img/overlay-bg.jpg)">
    <div class="overlay-mf"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div id="testimonial-mf" class="owl-carousel owl-theme">
            <div class="testimonial-box">
              <div class="author-test">
                  <img src="img/journey1.jpg" alt="" class="rounded-circle b-shadow-a">
              </div>
              <span class="author">HER UNEXPECTED JOURNEY</span>
              <div class="content-test">
                <p class="description lead">
                  She was born in Hisar District, Haryana, India to Mr. Mr. Rajesh Pathak who is an Electronics Businessman and Mrs. Aarti Pathak. She was named as Shivangi Pathak at birth, but now she is known as “The Eagle of Mountains” as he entered the world of professional mountaineering by climbing the Mt. Everest from the Nepal side on 16th May, 2018
                </p>
                <span class="comit"><i class="fa fa-quote-right"></i></span>
              </div>
            </div>
            <div class="testimonial-box">
              <div class="author-test">
                <img src="img/journey2.png" alt="" class="rounded-circle b-shadow-a">
              </div>
              <span class="author">Her dreams and inspiration</span>
              <div class="content-test">
                <p class="description lead">
                  Her journey of mountain climbing started with this incident a couple of years ago where she playfully asked her mom why she couldn’t find anything about herself when typed her name on Google search to which her mother responded that only people at the peak can find themselves on Google. She took that literally!
                </p>
                <span class="comit"><i class="fa fa-quote-right"></i></span>
              </div>
            </div>
            <div class="testimonial-box">
              <div class="author-test">
                <img src="img/journey3.png" alt="" class="rounded-circle b-shadow-a">
              </div>
              <span class="author">Her Motivation & Dedication</span>
              <div class="content-test">
                <p class="description lead">
                  Her curiosity in mountain climbing came after she saw the news of Arunima Sinha (first Indian amputee to climb Mt. Everest) This provoked her to start her journey to take up mountaineering and achieving similar laurels for herself. The challenge in pursuing mountaineering as a career started from home and society as she finished major training without half of her 11 membered family knowing about the training process.
                </p>
                <span class="comit"><i class="fa fa-quote-right"></i></span>
              </div>
            </div>
            <div class="testimonial-box">
              <div class="author-test">
                <img src="img/journey4.png" alt="" class="rounded-circle b-shadow-a">
              </div>
              <span class="author">Her family Sacrifices</span>
              <div class="content-test">
                <p class="description lead">
                  Her family was never expecting much from her instead it was expected from her brother to become a Doctor, who was her primary source of positivity throughout her training. Her father was initially hesitant to put her through such rigorous physical and mental training. It was only during their Rishikesh vacation, she did an 83 m bungee jumping at the age of 14 years surprising her parents, and it helped her win her father’s confidence and consent to kickstart her career in mountaineering.
                </p>
                <span class="comit"><i class="fa fa-quote-right"></i></span>
              </div>
            </div>
            <div class="testimonial-box">
              <div class="author-test">
                <img src="img/journey5.png" alt="" class="rounded-circle b-shadow-a">
              </div>
              <span class="author">Never give up</span>
              <div class="content-test">
                <p class="description lead">
                  Her parents were very supportive and made it a part of their life to help Shivangi achieve her goal. The family started saving money and cut down on expenses to provide her with adequate quality training & guidance. They even mortgaged their house to fund her expedition to Mt. Everest
                </p>
                <span class="comit"><i class="fa fa-quote-right"></i></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--/ Section Blog Star /-->
  <section id="blog" class="blog-mf sect-pt4 route">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-box text-center">
            <h3 class="title-a">
              Blog
            </h3>
            <p class="subtitle-a">
              Shivangi Pathak | Haryana | Mount Everest
            </p>
            <div class="line-mf"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="card card-blog">
            <div class="card-img">
              <a href="https://achievements-of-shivangi-pathak.blogspot.com/?m=1" target="_blank"><img src="img/blog1.jpg" alt="" class="img-fluid"></a>
            </div>
            <div class="card-body">
              <div class="card-category-box">
                <div class="card-category">
                  <h6 class="category">Travel</h6>
                </div>
              </div>
              <h3 class="card-title"><a href="blog-single.html">Success on Mount Kilimanjaro </a></h3>
              <p class="card-description">
                On July 2018, Shivangi has also scaled Mount Kilimanjaro [5] in just three days of climbing with her crew. She has started her journey from the Marangu route on July 21 and reached the summit on July 24, 2018.
              </p>
              <a href="https://achievements-of-shivangi-pathak.blogspot.com/?m=1" target="_blank"><p class="text-primary pull-right">View More</p></a>
            </div>

          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-blog">
            <div class="card-img">
              <a href="https://achievements-of-shivangi-pathak.blogspot.com/?m=1" target="_blank"><img src="img/blog2.jpg" alt="" class="img-fluid"></a>
            </div>
            <div class="card-body">
              <div class="card-category-box">
                <div class="card-category">
                  <h6 class="category">Experience</h6>
                </div>
              </div>
              <h3 class="card-title"><a href="blog-single.html">Few Facts of Mt Everest (8848 m)  </a></h3>
              <p class="card-description">
                Mountain Everest is the highest mountain on Earth with a height of 8,848.00 metres (29,028.87 ft). It is situated in the Himalayas, the highest mount range in the entire world.
              </p>
              <a href="https://achievements-of-shivangi-pathak.blogspot.com/?m=1" target="_blank"><p class="text-primary pull-right">View More</p></a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-blog">
            <div class="card-img">
              <a href="https://achievements-of-shivangi-pathak.blogspot.com/?m=1" target="_blank"><img src="img/blog3.jpg" alt="" class="img-fluid"></a>
            </div>
            <div class="card-body">
              <div class="card-category-box">
                <div class="card-category">
                  <h6 class="category">Sucess Story</h6>
                </div>
              </div>
              <h3 class="card-title"><a href="blog-single.html">Inspiration for Mountaineering </a></h3>
              <p class="card-description">
                She got inspiration take up mountaineering from her Mother. Once she searched Google and found nothing about herself and asked her mother playfully, “who get listed on Google.” Her Mother said, “JO PAHAD CHADTE HAIN” means who do great tasks in life. She took this phrase seriously and started searching a lot on mountaineering.
              </p>
              <a href="https://achievements-of-shivangi-pathak.blogspot.com/?m=1" target="_blank"><p class="text-primary pull-right">View More</p></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ Section Blog End /-->

  <!-- gallery start -->

  <section id="gallery" class="portfolio-mf sect-pt4 route">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="title-box text-center">
              <h3 class="title-a">
                Gallery
              </h3>
              <div class="line-mf"></div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-3">
            <div class="work-box cus-work-box">
              <a href="img/gallery/gallery-img16.jpg" data-lightbox="gallery-mf">
                <div class="work-img gallery-img">
                  <img src="img/gallery/gallery-img16.jpg" alt="" class="img-fluid">
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-3">
            <div class="work-box cus-work-box">
              <a href="img/gallery/gallery-img2.jpg" data-lightbox="gallery-mf">
                <div class="work-img gallery-img">
                  <img src="img/gallery/gallery-img2.jpg" alt="" class="img-fluid">
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-3">
            <div class="work-box cus-work-box">
              <a href="img/gallery/gallery-img3.jpg" data-lightbox="gallery-mf">
                <div class="work-img gallery-img">
                  <img src="img/gallery/gallery-img3.jpg" alt="" class="img-fluid">
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-3">
            <div class="work-box cus-work-box">
              <a href="img/gallery/gallery-img4.jpg" data-lightbox="gallery-mf">
                <div class="work-img gallery-img">
                  <img src="img/gallery/gallery-img4.jpg" alt="" class="img-fluid">
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-3">
            <div class="work-box cus-work-box">
              <a href="img/gallery/gallery-img5.jpg" data-lightbox="gallery-mf">
                <div class="work-img gallery-img">
                  <img src="img/gallery/gallery-img5.jpg" alt="" class="img-fluid">
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-3">
            <div class="work-box cus-work-box">
              <a href="img/gallery/gallery-img6.jpg" data-lightbox="gallery-mf">
                <div class="work-img gallery-img">
                  <img src="img/gallery/gallery-img6.jpg" alt="" class="img-fluid">
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-3">
            <div class="work-box cus-work-box">
              <a href="img/gallery/gallery-img17.jpg" data-lightbox="gallery-mf">
                <div class="work-img gallery-img">
                  <img src="img/gallery/gallery-img17.jpg" alt="" class="img-fluid">
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-3">
            <div class="work-box cus-work-box">
              <a href="img/gallery/gallery-img8.jpg" data-lightbox="gallery-mf">
                <div class="work-img gallery-img">
                  <img src="img/gallery/gallery-img8.jpg" alt="" class="img-fluid">
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-3">
            <div class="work-box cus-work-box">
              <a href="img/gallery/gallery-img21.jpg" data-lightbox="gallery-mf">
                <div class="work-img gallery-img">
                  <img src="img/gallery/gallery-img21.jpg" alt="" class="img-fluid">
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-3">
            <div class="work-box cus-work-box">
              <a href="img/gallery/gallery-img23.jpg" data-lightbox="gallery-mf">
                <div class="work-img gallery-img">
                  <img src="img/gallery/gallery-img23.jpg" alt="" class="img-fluid">
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-3">
            <div class="work-box cus-work-box">
              <a href="img/gallery/gallery-img24.jpg" data-lightbox="gallery-mf">
                <div class="work-img gallery-img">
                  <img src="img/gallery/gallery-img24.jpg" alt="" class="img-fluid">
                </div>
              </a>
            </div>
          </div>
          <div class="col-md-3">
            <div class="work-box cus-work-box">
              <a href="img/gallery/gallery-img26.jpg" data-lightbox="gallery-mf">
                <div class="work-img gallery-img">
                  <img src="img/gallery/gallery-img26.jpg" alt="" class="img-fluid">
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>

  <!-- gallery end -->

  <!--/ Section Contact-Footer Star /-->
  <section class="paralax-mf footer-paralax bg-image sect-mt4 route" style="background-image: url(img/overlay-bg.jpg)">
    <div class="overlay-mf"></div>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="contact-mf">
            <div id="contact" class="box-shadow-full">
              <div class="row">
                <div class="col-md-6">
                  <div class="title-box-2">
                    <h5 class="title-left">
                      Send Message Us
                    </h5>
                  </div>
                  <div>
                      <form role="form">
                      <div id="sendmessage">Your message has been sent. Thank you!</div>
                      <div id="errormessage"></div>
                      <div class="row">
                        <div class="col-md-12 mb-3">
                          <div class="form-group">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            <div class="validation"></div>
                          </div>
                        </div>
                        <div class="col-md-12 mb-3">
                          <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                            <div class="validation"></div>
                          </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div class="form-group">
                              <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                              <div class="validation"></div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                          <div class="form-group">
                            <textarea class="form-control" id="message" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                            <div class="validation"></div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <button id="contact_me" class="button button-a button-big button-rouded">Send Message</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="title-box-2 pt-4 pt-md-0">
                    <h5 class="title-left">
                      Get in Touch
                    </h5>
                  </div>
                  <div class="more-info">
                    <p class="lead">
                      Shivangi Pathak (aka The Eagle of Mountains) born on 10th July 2001, is an Indian mountaineer from Hisar District of Haryana.
                    </p>
                    <ul class="list-ico">
                      <li><span class="ion-ios-location"></span> Global Spaces Sector-24 , Hishar Haryana</li>
                      <li><span class="ion-ios-telephone"></span>9253951503</li>
                      <li><span class="ion-email"></span>everestgirl16@gmail.com <br> everestgirl16@gmail.com</li>
                    </ul>
                  </div>
                  <div class="socials">
                    <ul>
                      <li><a href="https://www.facebook.com/everestershivangipathak/"><span class="ico-circle"><i class="ion-social-facebook"></i></span></a></li>
                      <li><a href="https://instagram.com/shivangi___pathak?utm_source=ig_profile_share&igshid=4yj8z03ztxdz"><span class="ico-circle"><i class="ion-social-instagram"></i></span></a></li>
                      <li><a href="https://twitter.com/Shivangi_pathkk?s=08"><span class="ico-circle"><i class="ion-social-twitter"></i></span></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="copyright-box">
              <p class="copyright text-center">&copy; Copyright <strong>Pennybase Technology</strong>. All Rights Reserved</p>
              <div class="credits">

                Designed by <a href="#">Pennybase</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </section>
  <!--/ Section Contact-footer End /-->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader"></div>

  <!-- modals start -->
  <!-- The Modal Mt Everest (8848 m) -->
    <div class="modal" id="EverestModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">India Youngest Everest Mt Everest (8848 m) Summiter</h4>
            <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          </div>
          <!-- Modal body -->
          <div class="modal-body">

            <p>Mountain Everest is the highest mountain on Earth with a height of 8,848.00 metres (29,028.87 ft). It is situated in the Himalayas, the highest mount range in the entire world.</p>
            <p>The peak is situated on the border of Nepal and China. The summit is present above a Death Zone, the point where the air is too thin for a human being to live, which is 7, 600 metres (24, 900 ft) above the sea level. So, compressed gas cylinders consisting of different gas mixes for various altitudes are used in the climbing.</p>
            <p>There are two “Base Camps” present on North and South part of the Everest. The South Base Camp is in Nepal at the height of 5, 364 metres (17, 598 ft) and North Base Camp is in Tibet at 5, 150 metres (16, 896 ft).</p>
            <p>These camps are used by mountaineers to get supplies, rest and heal to prepare for their trip, in the up and down journey. Staying in these base camps help climbers to adjust their body to the thin air to reduce risk and altitude sickness. Many Sherpas are involved in shipping supplies to the South Base camp using Yaks.</p>
            <p>The area has an adverse climatic condition. It remained frigid and covered with snow with wind can speed up to 177 mph (285 km/h). </p>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>

        </div>
      </div>
    </div>
  </div>
<!-- The Modal Mt Everest (8848 m) end-->

<!-- The Modal Kilimanjaro -->
  <div class="modal" id="kilimanjaroModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">India Youngest Everest Mt Kilimanjaro(5895m) Summiter</h4>
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <p>On July 2018, Shivangi has also scaled Mount Kilimanjaro [5] in just three days of climbing with her crew. She has started her journey from the Marangu route on July 21 and reached the summit on July 24, 2018. It is Africa’s highest point and tallest free-standing mountain in the world situated in North-eastern Tanzania with a height of 5, 895 metres.</p>
          <p>From the “Roof of Africa,” she had sent a special message of “Beti Bachao, Beti Padhao” and women empowerment that they can battle and overcome odds with willpower. </p>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
  </div>
<!-- The Modal Kilimanjaro end-->

<!-- The Modal Elbrus -->
  <div class="modal" id="ElbrusModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">India Youngest Everest Mt Elbrus(5642m) Summiter</h4>
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <p>On 4th September 2018, she had completed her third summit to Mount Elbrus [6], which is the highest mountain in Europe with a height of 5642 meters. It is situated in the Caucasus Range of Russia and the 10th most prominent peak in the world. </p>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
  </div>
<!-- The Modal Elbrus end-->

<!-- The Modal President -->
  <div class="modal" id="PresidentModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Awarded By Bal shakti Puraskar By President Ram Nath Kovind</h4>
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <p>The award, earlier known as the National Child Award, was previously organised by an NGO, the Indian Council for Child Welfare (ICCW). From this year, the selection of children for the award has been brought under the Women and Child Development (WCD) Ministry.President Ram Nath Kovind presents Bal Shakti Puraskar 2019 in the field of sports to Kumari Shivangi Pathak during the Pradhan Mantri Rashtriya Bal Puraskar 2019 function at Rashtrapati Bhavan in New Delhi on Jan 22, 2019. </p>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
  </div>
<!-- The Modal President end-->

<!-- The Modal Prime Minister  -->
  <div class="modal" id="PrimeMinisterModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Honoured By Prime Minister Narendra Modi</h4>
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <p>Modi, in the 44th edition of his monthly radio address "Mann Ki Baat", said: "These Ashram School students began training in August 2017, covering Wardha, Hyderabad, Darjeeling and Leh-Ladakh. These young boys and girls had been selected under 'Mission Shaurya'. True to its name, they brought glory to the country with their brave deed of conquering the Everest."Prime Minister Narendra Modi on Sunday congratulated five tribal students for scaling Mount Everest and also praised the all-women crew of INSV Tarini for circumventing the globe.</p>
          <p>The tribal students -- Maneesha Dhurve, Pramesh Ale, Umakant Madhavi, Kavidas Katmode and Vikas Soyam -- from an Ashram School in Maharashtra's Chandrapur, scaled the world's highest peak on May 16..</p>
          <p>Modi, in the 44th edition of his monthly radio address "Mann Ki Baat", said: "These Ashram School students began training in August 2017, covering Wardha, Hyderabad, Darjeeling and Leh-Ladakh. These young boys and girls had been selected under 'Mission Shaurya'. True to its name, they brought glory to the country with their brave deed of conquering the Everest."
          </p>
          <p>He also praised several others, including 16-year-old Shivangi Pathak, for scaling the Everest.
          </p>
          <p>Pathak became the youngest Indian woman to scale Everest from the Nepal side.
          </p>
          <p>"For centuries, Everest has been throwing the gauntlet at humankind. And for long, brave hearts have been responding to the challenge," he said.
          </p>
          <p>He spoke about Ajit Bajaj and his daughter -- the first ever father-daughter duo to ascend Everest.</p>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
  </div>
<!-- The Modal Prime Minister -->

<!-- The Modal Chief Minister haryana  -->
  <div class="modal" id="CmharyanaModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Honoured And Appreciated by Chief Minister haryana</h4>
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <p>A 16-year-old girl from Haryana has registered her name as the youngest woman to scale the world highest peak on Thursday evening, according to local media.
          </p>
          <p>Born in Hisar of Haryana in north-western India, Shivangi Pathak climbed Mount Everest exploring a message that women could overcome every obstacle to achieve their goals.
          </p>
          <p>Pathak said that she was inspired by Arunima Sinha, who was the first Indian amputee to scale the nearly 29,000 ft peak."Javier Camacho (Spain) and Jangbu Sherpa also climbed Mt. Everest this morning," Thaneswar Guragai of Seven Summit Treks was quoted by the Himalayan Times as saying. He added, "The chairman at Pioneer Adventure Pvt. Ltd shared that Szczepan Janusz Brzeski and Magdalena Katarzyna of Poland, Bhagwan Chawale and Prajit Pardesi from India as well as Phunjo Jangmu Lama and Diki Sherpa scaled Mt. Everest this morning."
          </p>
          <p>"Climbing guides including Ngima Dorchi Sherpa, Raj Kumar Tamang, Pasang Tenji Sherpa, Dorchi Sherpa, Dawa Sherpa and Kami Sherpa also stood atop the mountain accompanying the expedition members," Guragai further said.
          </p>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
  </div>
<!-- The Modal Chief Minister haryana -->

<!-- media script start -->
<script type="text/javascript">
//FEATURED HOVER
$(document).ready(function(){
    $(".linkfeat").hover(
      function () {
          $(".textfeat").show(500);
      },
      function () {
          $(".textfeat").hide(500);
      }
  );
});

///contact me//////

    $("#contact_me").on('click',function(e){

        e.preventDefault();
        var name=$("#name").val();
        var email=$("#email").val();
        var sub=$("#subject").val();
        var message=$("#message").val();

        $.ajax({
        "type":"POST",
        "url":"contact.php",
        "data":"&n="+name+"&e="+email+"&s="+sub+"&m="+message,
        "dataType":"text",
        success:function(result){
           if(result=="success")
           {
             console.log('done');
             toastr.success('Thank you! Your request has been successfully submitted.');

           }
           else if(result=="error")
           {
             toastr.error('Mail not Sent');

           }
           else if(result=="email_error")
           {

             toastr.error('Invalid email');

           }
           else if(result=="email_error_valid")
           {
            toastr.error('Invalid phone number');
           }
        }
        })

    })

</script>
<!-- media script end -->
  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/popper/popper.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/counterup/jquery.waypoints.min.js"></script>
  <script src="lib/counterup/jquery.counterup.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <script src="lib/typed/typed.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <!-- <script src="contactform/contactform.js"></script> -->

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
